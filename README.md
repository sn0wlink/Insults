# Insults
A easy to implement, PHP random insulter library.

## Installation
Copy the files to your library and run the function. That's it!
  
## Practical Applications
- Display a 'Insult of the Day' on your website.
- Incorrect password entry
- Desktop electron app
- Add something unique to your 404 Error page
The possibiltys are endless...


### Licence
GNU General Public License v2.0
